package sadovnikov;

import javafx.fxml.FXML;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField fahrenheitTF;

    @FXML
    private TextField celsiusTF;

    @FXML
    private Button convertButton;

    @FXML
    void convertClicked() {
        double fahrenheit;
        double celsius;

        String celsiusString = "Fahrenheit temperature needed";
        String fahrenheitString = fahrenheitTF.getText();

        fahrenheit = Double.parseDouble(fahrenheitString);
        celsius = (fahrenheit - 32) * (5.0 / 9.0);
        celsiusString = Double.toString(celsius);

        celsiusTF.setText(celsiusString);
    }

    @FXML
    void initialize() {
    }
}
